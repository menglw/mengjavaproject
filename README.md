README.md

README for Java project.


1)Summary:
This project has code that will take files and be able to read them.
This project contains a class diagram.

There will be two kinds of code, one is the class code that is called WordCounter, it will have some functions. The other one is Driver, where the main code will run.


2) Map of folder:
In the src folder, there is a .gitignore file, README.md, and will have code to run the Java project. There will be a class code and a main code.

3)How to use:
-Netbeans to run code. Will have class and driver to run the main.
  -To run the code in Netbeans, run it through Driver
-For command line"
  "/Users/liwei/Desktop/MengJavaProject/dist/MengJavaProject.jar"
-Class diagram that maps out the code to this project.
